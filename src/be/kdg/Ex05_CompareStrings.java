package be.kdg;

public class Ex05_CompareStrings {
    public static void main(String[] args) {
        String stringOne = "java";
        String stringTwo = "Jakarta";
        String stringThree = "JAVA";

        //TODO: compare the strings to check which equal
        //      (without using equals)
        if (stringOne.compareToIgnoreCase(stringTwo) == 0) {
            System.out.println(stringOne + " and " + stringTwo + " are equal");
        } else {
            System.out.println(stringOne + " and " + stringTwo + " are not equal");
        }
        if (stringOne.compareToIgnoreCase(stringThree) == 0) {
            System.out.println(stringOne + " and " + stringThree + " are equal");
        } else {
            System.out.println(stringOne + " and " + stringThree + " are not equal");
        }
        if (stringTwo.compareToIgnoreCase(stringThree) == 0) {
            System.out.println(stringTwo + " and " + stringThree + " are equal");
        } else {
            System.out.println(stringTwo + " and " + stringThree + " are not equal");
        }
    }
}
