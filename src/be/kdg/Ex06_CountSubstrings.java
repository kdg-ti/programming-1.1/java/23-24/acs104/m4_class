package be.kdg;

public class Ex06_CountSubstrings {
    public static void main(String[] args) {
        final String TO_MATCH = "ou";
        String str = "The more you learn, the less you know.";
        int fromIndex = 0;
        int cnt = 0;

        // Option 1
        int index = 0;
        cnt = 0;
        while(0 <= index) {
            index = str.indexOf(TO_MATCH, fromIndex);
            if (0 <= index) {
                cnt++;
            }
            fromIndex = index + 1;
        }
        System.out.println("#1 Count: " + cnt);

        // Option 2
        cnt = 0;
        do {
            fromIndex = str.indexOf(TO_MATCH, fromIndex);
            if (0 <= fromIndex) {
                cnt++;
                fromIndex = fromIndex + 1;
            }
        } while(0 < fromIndex);

        System.out.println("#2 Count: " + cnt);
    }
}
