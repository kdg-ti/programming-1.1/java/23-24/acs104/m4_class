package be.kdg;

import java.util.Scanner;
public class Ex04_Alphabetical {
    public static void main(String[] args) {
        String line1;
        String line2;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a text: ");
        line1 = keyboard.nextLine();
        System.out.print("Enter another text: ");
        line2 = keyboard.nextLine();

        line1 = line1.trim();
        line2 = line2.trim();

        if (line1.compareTo(line2) < 0) {
            System.out.print(line1 + " " + line2);
        } else {
            System.out.println(line2 + " " + line1);
        }
    }
}
